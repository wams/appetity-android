package moun.com.deli.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.appcompat.BuildConfig;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.adapters.ScaleInAnimationAdapter;
import moun.com.deli.Conection.DataCallback;
import moun.com.deli.HotDealsActivity;
import moun.com.deli.Libs.AlmacenamientoLocal;
import moun.com.deli.Libs.ConnectionDetector;
import moun.com.deli.MainActivity;
import moun.com.deli.Parse.Parse_categories;
import moun.com.deli.Parse.Parse_deals;
import moun.com.deli.Parse.Parse_review;
import moun.com.deli.R;
import moun.com.deli.adapter.HomeMenuCustomAdapter;
import moun.com.deli.adapter.HotdealsViewPagerAdapter;
import moun.com.deli.model.MenuItems;
import moun.com.deli.model.Restaurants;
import moun.com.deli.model.Review;
import moun.com.deli.util.AppUtils;

/**
 * This Fragment used to handle the list of items with header on the top.
 */
public class MainFragment extends Fragment implements HomeMenuCustomAdapter.ClickListener,DataCallback {

    public static String id_res;
    public static String name_res;
    ViewPager pager;
    private String dataLocation;
    private TextView navigator;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private static final int SPAN_COUNT = 2;
    private HomeMenuCustomAdapter homeMenuCustomAdapter;
    ArrayList<MenuItems> rowListItem;
    private boolean mLinearShown;
    LayoutInflater inflater;
    private TextView startOrder;
    private AlphaInAnimationAdapter alphaAdapter;
    View header;
    private TextView hotDealheaderText;
    private OnItemSelectedListener listener;
    HotdealsViewPagerAdapter mAdapter;
    private AlmacenamientoLocal cache;
    JSONArray jsondata;
    JSONArray jsondataDeals;
    JSONArray jsonReview;
    ArrayList<MenuItems> menuItems = new ArrayList<MenuItems>();
    ArrayList<MenuItems> menuItemsDeals = new ArrayList<MenuItems>();
    ArrayList<Review> reviewRestaurant;
    ConnectionDetector conexion;
    Parse_categories parseM;
    String positionRes;
    Parse_deals parseD;
    Parse_review parseR;
    ProgressBar progress;
    ArrayList<Restaurants> restaurantesModel = new ArrayList<>();
    public static JSONArray datosCate;
    public static JSONArray datosDeals;
    public static JSONArray datosComents;
    public static String url;


    /**
     * Callback used to communicate with MainFragment to Determine the Current Layout.
     * MainActivity implements this interface and communicates with MainFragment.
     */
    @Override
    public void callback(String data) {
        /*if (!data.equals("") && data != null && data.indexOf("Exception") < 0) {
            try {
                jsondata = new JSONObject(data);
                MainActivity.data = jsondata;
                rowListItem= getMenuList(jsondata);
                cache.setValuesriablePermanente("Restaurant", jsondata.toString());
                cargadecontenido();
            } catch (Exception ex) {
                // Analytics.logError("JuegosFragment.callback()", ex.getMessage(), ex.getCause());
            }
        }*/
    }

    public interface OnItemSelectedListener {
        public void onItemSelected(int position);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        if (context instanceof OnItemSelectedListener) {
            listener = (OnItemSelectedListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement OnItemSelectedListener");
        }
    }

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    protected LayoutManagerType mCurrentLayoutManagerType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //rowListItem = getMenuList();
    }

    public void setValues(){
        /*Bundle b = getActivity().getIntent().getExtras();
        if(b!=null){
            String auxCate = b.getString("datosRestaurantCate");
            String auxDeals = b.getString("datosRestaurantDeals");
            dataLocation = b.getString("datosRestaurantLocation");
            try {
                JSONArray jsonCate = new JSONArray(auxCate);
                JSONArray jsonDeals = new JSONArray(auxDeals);
                datosCate = jsonCate;
                datosDeals= jsonDeals;
            }catch (JSONException e){
                e.printStackTrace();
            }
        }*/
        Bundle b = getActivity().getIntent().getExtras();
        if(b!=null){
            positionRes = b.getString("posRestaurantes");
        }
        restaurantesModel = (ArrayList<Restaurants>)getActivity().getIntent().getSerializableExtra("datosRestaurantes");
        try {
            for(int i=0; i < restaurantesModel.size();i++){
                if(restaurantesModel.get(i).getRestaurantName().equals(positionRes)){
                    String cate = restaurantesModel.get(i).getRestaurantCategories();
                    String deals = restaurantesModel.get(i).getRestaurantDeals();
                    String coments = restaurantesModel.get(i).getRestaurantReview();
                    JSONArray jsonCate = new JSONArray(cate);
                    JSONArray jsonDeals = new JSONArray(deals);
                    JSONArray jsonReview = new JSONArray(coments);
                    datosCate = jsonCate;
                    datosDeals= jsonDeals;
                    datosComents = jsonReview;
                    break;
                }
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
    }
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment
        // The layout file is defined in the project res/layout/fragment_main.xml file
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        View rv = inflater.inflate(R.layout.single_row_grid_layout,container,false);
        View v = inflater.inflate(R.layout.drawer_header,container,false);
        this.inflater = inflater;
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        setValues();
        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
        startOrder = (TextView) rootView.findViewById(R.id.start_order_text);
        startOrder.setTypeface(AppUtils.getTypeface(getActivity(), AppUtils.FONT_BOLD));
        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);


        //*****************************************************************************
        parseM = new Parse_categories();
        parseD = new Parse_deals();
        parseR = new Parse_review();
       /*
        conexion = new ConnectionDetector(getActivity());
        cache = new AlmacenamientoLocal(getActivity());
        if (conexion.hasInternet(getActivity())) {
            // ActionBarPullToRefresh.from(getActivity()).allChildrenArePullable().listener(this).setup(mPullToRefreshLayout);
            try {
                progress = (ProgressBar) rootView.findViewById(R.id.progressBar_principal);
                progress.setVisibility(View.VISIBLE);
                ConexionHTTP.getWebData(Configuracion.JSON_RESTAURANT,this);
            } catch (Exception e) {
                e.printStackTrace();
                // Analytics.logError("JSONget JuegosFragment", e.getMessage(), e.getCause());
            }
        } else {
            if (cache.getVariablePermanente("Restaurant") == null || cache.getVariablePermanente("Restaurant").equals("")) {
                //Toast.makeText(getActivity(), getResources().getString(R.string.toast2), Toast.LENGTH_LONG).show();
            } else {
                try {
                    jsondata = new JSONObject(cache.getVariablePermanente("Restaurant"));
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                rowListItem= getMenuList(jsondata);
                MainActivity.data = jsondata;
                cargadecontenido();
            }
        }*/

        //***************************************************************************

        ///rowListItem= getMenuList(jsondata);
        jsondata = datosCate;
        jsondataDeals = datosDeals;
        jsonReview = datosComents;
        rowListItem = getMenuList(jsondata,jsondataDeals,jsonReview);
        cargadecontenido();
        MainActivity.data = menuItems;
        MainActivity.deals = menuItemsDeals;
        MainActivity.review = reviewRestaurant;

        return rootView;

    }

    public void cargadecontenido(){
        String text = "No hay internet";
        if (jsondata == null) {
            Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
        }
         mAdapter = new HotdealsViewPagerAdapter(getActivity(),getFragmentManager(), datosDeals);
        // Inflate the layout header
        header = LayoutInflater.from(getActivity()).inflate(R.layout.home_menu_header, mRecyclerView, false);
        hotDealheaderText = (TextView) header.findViewById(R.id.hot_deal_header_title);
        pager = (ViewPager)header.findViewById(R.id.viewpager_hotdeals);
        this.navigator = (TextView) header.findViewById(R.id.activity_wizard_universal_possition);
         //  HotdealsViewPagerAdapter mAdapter = new HotdealsViewPagerAdapter(getActivity(),getFragmentManager(),);
        pager.setAdapter(mAdapter);
        setNavigator();
        this.pager.setOnPageChangeListener(new onPageChange());
        pager.setCurrentItem(0);
        hotDealheaderText.setTypeface(AppUtils.getTypeface(getActivity(), AppUtils.FONT_BOLD));
        homeMenuCustomAdapter = new HomeMenuCustomAdapter(getActivity(), header, rowListItem, inflater, R.layout.single_row_grid_layout);
        alphaAdapter = new AlphaInAnimationAdapter(homeMenuCustomAdapter);
        // Set HomeMenuCustomAdapter as the adapter for RecyclerView.
      //  progress.setVisibility(View.GONE);
        mRecyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));
        homeMenuCustomAdapter.setClickListener(this);
    }

    @Override
    public void itemClicked(View view, int position) {
        if (position == 0) {
            // user click on the header(hot deals section), start the HotDealsActivity.
            Intent intent = new Intent(getActivity(), HotDealsActivity.class);
            startActivity(intent);
        } else {
            // Send the event to the main activity
          /*  try{
                MainActivity.data = new JSONArray(jsondata);
            }catch (JSONException e){
                e.printStackTrace();
            }*/
            //categories =  parseM.parseData_categories(jsondata);
            listener.onItemSelected(position);
        }
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_menu_transition, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // We change the look of the icon every time the user toggles between list and grid layout.
        switch (item.getItemId()) {
            case R.id.action_toggle: {
                mLinearShown = !mLinearShown;
                if (mLinearShown) {
                    setRecyclerViewLayoutManager(LayoutManagerType.LINEAR_LAYOUT_MANAGER);
                    homeMenuCustomAdapter = new HomeMenuCustomAdapter(getActivity(), header, rowListItem, inflater, R.layout.single_row_linear_layout);
                    alphaAdapter = new AlphaInAnimationAdapter(homeMenuCustomAdapter);
                    mRecyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));
                    homeMenuCustomAdapter.setClickListener(this);
                    item.setIcon(R.mipmap.ic_grid_on_white_24dp);

                } else {
                    setRecyclerViewLayoutManager(LayoutManagerType.GRID_LAYOUT_MANAGER);
                    homeMenuCustomAdapter = new HomeMenuCustomAdapter(getActivity(), header, rowListItem, inflater, R.layout.single_row_grid_layout);
                    alphaAdapter = new AlphaInAnimationAdapter(homeMenuCustomAdapter);
                    mRecyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));
                    homeMenuCustomAdapter.setClickListener(this);
                    item.setIcon(R.mipmap.ic_view_list_white_24dp);
                }

                return true;
            }
        }
        return false;
    }


    /**
     * Set RecyclerView's LayoutManager to the one given.
     *
     * @param layoutManagerType Type of layout manager to switch to.
     */
    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                final GridLayoutManager gridManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mLayoutManager = gridManager;
                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                // Override setSpanSizeLookup in GridLayoutManager to return the span count as the span size for the header
                gridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return homeMenuCustomAdapter.isHeader(position) ? gridManager.getSpanCount() : 1;
                    }
                });
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    /**
     * Generates data for RecyclerView's adapter, this data would usually come from a local content provider
     * or remote server.
     *
     * @return menu items list
     */
    private ArrayList<MenuItems> getMenuList(JSONArray data,JSONArray deals, JSONArray coments) {
        menuItems = parseM.parseData_categories(data);
        menuItemsDeals = parseD.parseData_Deals(deals);
        reviewRestaurant = parseR.parseReview(coments);
        return menuItems;
    }

    public  void setNavigator() {
        String navigation = BuildConfig.FLAVOR;
        for (int i = 0; i < this.mAdapter.getCount(); i++) {
            if (i == this.pager.getCurrentItem()) {
                navigation = new StringBuilder(String.valueOf(navigation)).append(getString(R.string.material_icon_point_full)).append("  ").toString();
            } else {
                navigation = new StringBuilder(String.valueOf(navigation)).append(getString(R.string.material_icon_point_empty)).append("  ").toString();
            }
        }
        this.navigator.setText(navigation);
    }

    class onPageChange implements ViewPager.OnPageChangeListener {
        onPageChange() {
        }

        public void onPageSelected(int position) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int position) {
            MainFragment.this.setNavigator();
        }
    }



}
