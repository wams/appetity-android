package moun.com.deli.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;

import moun.com.deli.HotDealsActivity;
import moun.com.deli.R;
import moun.com.deli.model.MenuItems;

/**
 * Created by WilliamO on 9/22/2016.
 */
public class HotDealsViewPagerFragment extends Fragment {
    JSONObject datosDeal;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_hotdealsviewpger, container, false);
        String type = getArguments().getString("imageDeal");
        //final String name_items_deals = getArguments().getString("jsonDatos");

        final ArrayList<MenuItems> listItems = getArguments().getParcelableArrayList("data");

        ImageView image_deals = (ImageView)v.findViewById(R.id.image_principal_deals);

        image_deals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HotDealsActivity fm = new HotDealsActivity();
                Intent intent = new Intent(getActivity(), HotDealsActivity.class);
                intent.putExtra("dataDeals",listItems);
                startActivity(intent);
            }
        });

        Glide.with(getActivity())
                .load(type)
                //.placeholder(R.drawable.)
                .into(image_deals);

        return v;
    }

    public static HotDealsViewPagerFragment newInstance(ArrayList<MenuItems> data,String image) {
        HotDealsViewPagerFragment f = new HotDealsViewPagerFragment();
        Bundle b = new Bundle();
        b.putString("imageDeal",image);
        b.putSerializable("data",data);
        //b.putString("jsonDatos",data.toString());
        //b.putStringArrayList("ArrayListDeals",data);
        // b.putString("dealImage", text);
        f.setArguments(b);
        return f;
    }
}
