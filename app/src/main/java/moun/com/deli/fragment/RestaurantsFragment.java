package moun.com.deli.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.adapters.ScaleInAnimationAdapter;
import moun.com.deli.Conection.ConexionHTTP;
import moun.com.deli.Conection.DataCallback;
import moun.com.deli.Libs.AlmacenamientoLocal;
import moun.com.deli.Libs.Configuracion;
import moun.com.deli.Libs.ConnectionDetector;
import moun.com.deli.LocationActivity;
import moun.com.deli.Parse.Parse_Restaurant;
import moun.com.deli.R;
import moun.com.deli.RestaurantsActivity;
import moun.com.deli.adapter.RestaurantAdapter;
import moun.com.deli.model.Restaurants;
import moun.com.deli.util.AppUtils;

/**
 * Created by WilliamO on 10/20/2016.
 */
public class RestaurantsFragment extends Fragment implements  RestaurantAdapter.ClickListener,DataCallback,SearchView.OnQueryTextListener {


    LayoutInflater inflater;
    ConnectionDetector conexion;
    private AlmacenamientoLocal cache;
    JSONObject jsondata;
    ArrayList<Restaurants> rowListItem;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private TextView startOrder;
    private OnItemSelectedListener listener;
    private static final int SPAN_COUNT = 2;
    ArrayList<Restaurants> restaurantList = new ArrayList<Restaurants>();
    Parse_Restaurant parseM;
    private AlphaInAnimationAdapter alphaAdapter;
    RestaurantAdapter mAdapterRestaurant;
    private ProgressBar progressBar;
    private boolean enableEfect = true;
    private boolean enableProgress = true;
    @Override
    public void callback(String data) {
        if (!data.equals("") && data != null && data.indexOf("Exception") < 0) {
            try {
                jsondata = new JSONObject(data);
                cache.setVariablePermanente("Restaurant", jsondata.toString());
                Log.i("DataPrueba",jsondata.toString());
                rowListItem = getMenuList(jsondata);
                progressBar.setVisibility(View.GONE);
                cargadecontenido();
            } catch (Exception ex) {
                // Analytics.logError("JuegosFragment.callback()", ex.getMessage(), ex.getCause());
            }
        }
    }

    /*
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final ArrayList<Restaurants> filteredModelList = filter(restaurantList, newText);
        mAdapterRestaurant.setFilter(filteredModelList);
        return true;
    }
    */
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }
    @Override
    public boolean onQueryTextChange(String newText) {
        ArrayList<Restaurants> restaurantList2 = new ArrayList<>();
        if (newText != null && newText.length() > 0) {
            for(int i=0; i < restaurantList.size(); i++){
                String name = restaurantList.get(i).getRestaurantName();
                String address = restaurantList.get(i).getRestaurantAddress();
                String total_filter = name + address;
                String cacac = newText.toLowerCase();
                if (total_filter.toLowerCase().contains(cacac)) {
                    restaurantList2.add(restaurantList.get(i));
                }
            }

            mAdapterRestaurant = new RestaurantAdapter(getActivity(),restaurantList2);
            mRecyclerView.setAdapter(mAdapterRestaurant);
            mAdapterRestaurant.setClickListener(this);
            //testString = "";
        } else {
            mAdapterRestaurant = new RestaurantAdapter(getActivity(),restaurantList);
            mRecyclerView.setAdapter(mAdapterRestaurant);
            mAdapterRestaurant.setClickListener(this);
            //testString = "";
        }
        return false;
    }
    public interface OnItemSelectedListener {
        public void onItemSelected(int position);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        if (context instanceof OnItemSelectedListener) {
            listener = (OnItemSelectedListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement OnItemSelectedListener");
        }
    }
    @Override
    public void itemClicked(View view, int position) {
            //RestaurantsActivity.dataCategories = restaurantList.get(position).getRestaurantCategories();
            //RestaurantsActivity.dataDeals = restaurantList.get(position).getRestaurantDeals();
            RestaurantsActivity.datos = restaurantList;
            listener.onItemSelected(position);
    }
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // The layout file is defined in the project res/layout/fragment_main.xml file
        View rootView = inflater.inflate(R.layout.fragment_restaurants, container, false);
        this.inflater = inflater;
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBarRes);
       /* sv = (SearchView)rootView.findViewById(R.id.search_viewToolbar);

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String text) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String text) {

                return false;
            }
        });*/
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView_restaurant);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
        startOrder = (TextView) rootView.findViewById(R.id.start_order_text);
        startOrder.setTypeface(AppUtils.getTypeface(getActivity(), AppUtils.FONT_BOLD));
        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
        /***********************************/
        parseM = new Parse_Restaurant();
        conexion = new ConnectionDetector(getActivity());
        cache = new AlmacenamientoLocal(getActivity());

        if(cache.getVariablePermanente("Restaurant") != null){
            try {
                jsondata = new JSONObject(cache.getVariablePermanente("Restaurant"));
                rowListItem = getMenuList(jsondata);
                enableEfect = false;
                enableProgress = false;
                cargadecontenido();
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }

        if (conexion.hasInternet(getActivity())) {
            try {
                if(enableEfect == true){
                    progressBar.setVisibility(View.VISIBLE);
                }
                if(enableProgress == true){
                    progressBar.setVisibility(View.VISIBLE);
                }
                ConexionHTTP.getWebData(Configuracion.JSON_RESTAURANT,this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (cache.getVariablePermanente("Restaurant") == null || cache.getVariablePermanente("Restaurant").equals("")) {
                Toast.makeText(getActivity(), "No hay internet", Toast.LENGTH_LONG).show();
            } else {
                try {
                    jsondata = new JSONObject(cache.getVariablePermanente("Restaurant"));
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                rowListItem = getMenuList(jsondata);
                cargadecontenido();
            }
        }
        return rootView;
    }

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    protected LayoutManagerType mCurrentLayoutManagerType;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //rowListItem = getMenuList();
    }

    /**
     * Set RecyclerView's LayoutManager to the one given.
     *
     * @param layoutManagerType Type of layout manager to switch to.
     */
    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;
        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }
        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                final GridLayoutManager gridManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mLayoutManager = gridManager;
                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                // Override setSpanSizeLookup in GridLayoutManager to return the span count as the span size for the header
              /*  gridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return homeMenuCustomAdapter.isHeader(position) ? gridManager.getSpanCount() : 1;
                    }
                });*/
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    private ArrayList<Restaurants> getMenuList(JSONObject data) {
        restaurantList = parseM.parse_Restaurant(data);
       /* for(int i = 0; i< restaurantList.size(); i++){
            restaurant.add(new Restaurants(restaurantList.get(i).get(parseM.TAG_NAME_RESTAURANT), restaurantList.get(i).get(parseM.TAG_LOGO_RESTAURANT)));
        } */
        /*menuItems.add(new MenuItems(getString(R.string.sandwich), R.drawable.items2));
        menuItems.add(new MenuItems(getString(R.string.burgers), R.drawable.items3));
        menuItems.add(new MenuItems(getString(R.string.pizza), R.drawable.items4));
        menuItems.add(new MenuItems(getString(R.string.salads), R.drawable.items5));
        menuItems.add(new MenuItems(getString(R.string.sweets), R.drawable.items7));
        menuItems.add(new MenuItems(getString(R.string.drinks), R.drawable.items6));*/
        return restaurantList;
    }

    public void cargadecontenido(){
        String text = "No hay internet";
        if (jsondata == null) {
            Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
        }
        if(enableEfect){
            restaurantList = parseM.parse_Restaurant(jsondata);
            mAdapterRestaurant = new RestaurantAdapter(getActivity(),restaurantList);
            alphaAdapter = new AlphaInAnimationAdapter(mAdapterRestaurant);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mRecyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));
            mAdapterRestaurant.setClickListener(this);
        }else{
            restaurantList = parseM.parse_Restaurant(jsondata);
            mAdapterRestaurant = new RestaurantAdapter(getActivity(),restaurantList);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mRecyclerView.setAdapter(mAdapterRestaurant);
            mAdapterRestaurant.setClickListener(this);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
         final MenuItem item = menu.findItem(R.id.action_search);
         final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setQueryHint("Food, city or name");
         searchView.setOnQueryTextListener(this);

         /*MenuItemCompat.setOnActionExpandListener(item,
                  new MenuItemCompat.OnActionExpandListener() {
              @Override
              public boolean onMenuItemActionCollapse(MenuItem item) {
                   // Do something when collapsed
                   mAdapterRestaurant.setFilter(restaurantList);
                   return true; // Return true to collapse action view
                  }

              @Override
              public boolean onMenuItemActionExpand(MenuItem item) {
                   // Do something when expanded
                   return true; // Return true to expand action view
                  }
              });*/
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getActivity(), LocationActivity.class);
                intent.putExtra("colorMap","#262425");
                intent.putExtra("booleanMap",true);
                intent.putExtra("listRes",restaurantList);
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
