package moun.com.deli.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.adapters.ScaleInAnimationAdapter;
import moun.com.deli.R;
import moun.com.deli.adapter.Adapter_review;
import moun.com.deli.model.Review;

/**
 * Created by WilliamO on 11/21/2016.
 */
public class ValorationFragment extends Fragment {
    private RatingBar mRatingbar;
    private TextView mTextRating;
    private EditText mEditText;
    private TextView mTextRatingCount;
    private RecyclerView mRecyclerView;
    private Adapter_review mAdapter;
    public static ArrayList<Review> comentarios;
    private Button buttonRating;
    private AlphaInAnimationAdapter alphaAdapter;

    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // The layout file is defined in the project res/layout/fragment_main.xml file
        View rootView = inflater.inflate(R.layout.fragment_valoration, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.listView_valoration);
        mRatingbar  = (RatingBar)rootView.findViewById(R.id.ratingBar_valoration);
        mTextRating = (TextView)rootView.findViewById(R.id.txt_rating);
        mTextRatingCount = (TextView)rootView.findViewById(R.id.txt_rating_count);
        mEditText   = (EditText)rootView.findViewById(R.id.edittext_ratting);
        mEditText.setEnabled(true);
        mEditText.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //This sets a textview to the current length
                mTextRatingCount.setText(String.valueOf(s.length())+"/160");

            }

            public void afterTextChanged(Editable s) {
            }

        });
        mRatingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if(rating<=1 ){
                    mTextRating.setText("It's awful");
                }else if(rating<=2 && rating>1){
                    mTextRating.setText("It's bad");
                }else if(rating<=3&& rating>2){
                    mTextRating.setText("It's good");
                }else if(rating<=4 && rating>3){
                    mTextRating.setText("It's nice");
                }else if(rating<=5 || rating>4 ){
                    mTextRating.setText("It's great");
                }
            }
        });

        buttonRating = (Button)rootView.findViewById(R.id.button_ratting);


        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new Adapter_review(getActivity(),comentarios );
        alphaAdapter = new AlphaInAnimationAdapter(mAdapter);
        mRecyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));


        return rootView;
    }
}
