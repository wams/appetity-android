package moun.com.deli.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by WilliamO on 10/21/2016.
 */
public class Restaurants implements Parcelable {


    private String id;
    private String restaurantName;
    private String restaurantAddress;
    private String restaurantLogo;
    private String restaurantLocationIcon;
    private String restaurantPhone;
    private String restaurantAperturaHora;
    private String restaurantCierreHora;
    private String restaurantCategories;
    private String restaurantDeals;
    private String restaurantLocation;
    private String restaurantColorBG;
    private String restaurantColorText;
    private String ratingBar;
    private String ratingUser;
    private String restaurantReview;

    public Restaurants() {
        super();
    }

    public Restaurants(String id , String resName, String resImage, String addres, String location, String phone, String hora_apertura, String hora_cierre, String categories, String deals, String location2, String colorbg, String colortext,String ratingBar, String ratingUser, String review) {
        this.id = id;
        this.restaurantName = resName;
        this.restaurantLogo = resImage;
        this.restaurantAddress = addres;
        this.restaurantLocationIcon = location;
        this.restaurantPhone = phone;
        this.restaurantAperturaHora = hora_apertura;
        this.restaurantCierreHora  = hora_cierre;
        this.restaurantCategories = categories;
        this.restaurantDeals      = deals;
        this.restaurantLocation   = location2 ;
        this.restaurantColorBG   = colorbg;
        this.restaurantColorText = colortext;
        this.ratingBar = ratingBar;
        this.ratingUser = ratingUser;
        this.restaurantReview = review;
    }

    public Restaurants(Parcel parcel) {
        super();
        this.id = parcel.readString();
        this.restaurantName = parcel.readString();
        this.restaurantLogo = parcel.readString();
        this.restaurantAddress = parcel.readString();
        this.restaurantLocationIcon = parcel.readString();
        this.restaurantPhone = parcel.readString();
        this.restaurantAperturaHora = parcel.readString();
        this.restaurantCierreHora = parcel.readString();
        this.restaurantCategories = parcel.readString();
        this.restaurantDeals = parcel.readString();
        this.restaurantLocation = parcel.readString();
        this.restaurantColorBG = parcel.readString();
        this.restaurantColorText= parcel.readString();
        this.ratingBar =parcel.readString();
        this.ratingUser = parcel.readString();
        this.restaurantReview = parcel.readString();
        }
    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(getId());
        parcel.writeString(getRestaurantName());
        parcel.writeString(getRestaurantLogo());
        parcel.writeString(getRestaurantAddress());
        parcel.writeString(getRestaurantLocationIcon());
        parcel.writeString(getRestaurantPhone());
        parcel.writeString(getRestaurantAperturaHora());
        parcel.writeString(getRestaurantCierreHora());
        parcel.writeString(getRestaurantCategories());
        parcel.writeString(getRestaurantDeals());
        parcel.writeString(getRestaurantLocation());
        parcel.writeString(getRestaurantColorBG());
        parcel.writeString(getRestaurantColorText());
        parcel.writeString(getRatingBar());
        parcel.writeString(getRatingUser());
        parcel.writeString(getRestaurantReview());
            }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getRestaurantColorBG() {
        return restaurantColorBG;
    }
    public void setRestaurantColorBG(String restaurantColorBG) {
        this.restaurantColorBG = restaurantColorBG;
    }
    public String getRestaurantColorText() {
        return restaurantColorText;
    }
    public String getRatingBar() {
        return ratingBar;
    }
    public void setRatingBar(String ratingBar) {
        this.ratingBar = ratingBar;
    }
    public String getRatingUser() {
        return ratingUser;
    }
    public void setRatingUser(String ratingUser) {
        this.ratingUser = ratingUser;
    }
    public void setRestaurantColorText(String restaurantColorText) {
        this.restaurantColorText = restaurantColorText;
    }
    public String getRestaurantCategories() {
        return restaurantCategories;
    }
    public void setRestaurantCategories(String restaurantCategories) {
        this.restaurantCategories = restaurantCategories;
    }
    public String getRestaurantDeals() {
        return restaurantDeals;
    }
    public void setRestaurantDeals(String restaurantDeals) {
        this.restaurantDeals = restaurantDeals;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getRestaurantName() {
        return restaurantName;
    }
    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }
    public String getRestaurantAddress() {
        return restaurantAddress;
    }
    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }
    public String getRestaurantLogo() {
        return restaurantLogo;
    }
    public void setRestaurantLogo(String restaurantLogo) {
        this.restaurantLogo = restaurantLogo;
    }
    public String getRestaurantLocationIcon() {
        return restaurantLocationIcon;
    }
    public void setRestaurantLocationIcon(String restaurantLocationIcon) {
        this.restaurantLocationIcon = restaurantLocationIcon;
    }
    public String getRestaurantPhone() {
        return restaurantPhone;
    }
    public void setRestaurantPhone(String restaurantPhone) {
        this.restaurantPhone = restaurantPhone;
    }
    public String getRestaurantAperturaHora() {
        return restaurantAperturaHora;
    }
    public void setRestaurantAperturaHora(String restaurantAperturaHora) {
        this.restaurantAperturaHora = restaurantAperturaHora;
    }
    public String getRestaurantCierreHora() {
        return restaurantCierreHora;
    }
    public void setRestaurantCierreHora(String restaurantCierreHora) {
        this.restaurantCierreHora = restaurantCierreHora;
    }
    public String getRestaurantLocation() {
        return restaurantLocation;
    }
    public void setRestaurantLocation(String restaurantLocation) {
        this.restaurantLocation = restaurantLocation;
    }
    public String getRestaurantReview() {
        return restaurantReview;
    }
    public void setRestaurantReview(String restaurantReview) {
        this.restaurantReview = restaurantReview;
    }

    @Override
    public String toString() {
        return "Restaurants{" +
                "restaurantName='" + restaurantName + '\'' +
                ", restaurantAddress='" + restaurantAddress + '\'' +
                ", restaurantLogo='" + restaurantLogo + '\'' +
                ", restaurantLocationIcon='" + restaurantLocationIcon + '\'' +
                ", restaurantPhone='" + restaurantPhone + '\'' +
                ", restaurantAperturaHora='" + restaurantAperturaHora + '\'' +
                ", restaurantCierreHora='" + restaurantCierreHora + '\'' +
                ", restaurantCategories='" + restaurantCategories + '\'' +
                ", restaurantDeals='" + restaurantDeals + '\'' +
                ", restaurantLocation='" + restaurantLocation + '\'' +
                ", restaurantColorBG='" + restaurantColorBG + '\'' +
                ", restaurantColorText='" + restaurantColorText + '\'' +
                ", ratingBar='" + ratingBar + '\'' +
                ", ratingUser='" + ratingUser + '\'' +
                ", restaurantReview='" + restaurantReview + '\'' +
                '}';
    }

    public static final Parcelable.Creator<Restaurants> CREATOR = new Parcelable.Creator<Restaurants>() {
        public Restaurants createFromParcel(Parcel in) {
            return new Restaurants(in);
        }

        public Restaurants[] newArray(int size) {
            return new Restaurants[size];
        }
    };

}
