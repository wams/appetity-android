package moun.com.deli.model;

import android.os.Parcel;
import android.os.Parcelable;


public class MenuItems implements Parcelable {

    private int id;
    private String itemImage;
    private String itemName;
    private double itemPrice;
    private String itemDescription;
    private int itemQuantity;
    private String itemCategories;

    public MenuItems() {
        super();
    }


    public MenuItems(String itemName, String itemImage,String itemsCategorie) {
        this.itemName = itemName;
        this.itemImage = itemImage;
        this.itemCategories = itemsCategorie;
    }

    public MenuItems(int itemId, String itemName, String itemImage, double itemPrice, String itemDescription) {
        this.id = itemId;
        this.itemName = itemName;
        this.itemImage = itemImage;
        this.itemPrice = itemPrice;
        this.itemDescription = itemDescription;
    }

    public MenuItems(Parcel parcel) {
        super();
        this.id = parcel.readInt();
        this.itemName = parcel.readString();
        this.itemImage = parcel.readString();
        this.itemPrice = parcel.readDouble();
        this.itemDescription = parcel.readString();
        this.itemQuantity = parcel.readInt();
        this.itemCategories = parcel.readString();
    }

    public String getItemCategories() {
        return itemCategories;
    }
    public void setItemCategories(String itemCategories) {
        this.itemCategories = itemCategories;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public int getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(getId());
        parcel.writeString(getItemName());
        parcel.writeString(getItemImage());
        parcel.writeDouble(getItemPrice());
        parcel.writeString(getItemDescription());
        parcel.writeInt(getItemQuantity());
        parcel.writeString(getItemCategories());
    }

    @Override
    public String toString() {
        return "MenuItems{" +
                "id=" + id +
                ", itemImage=" + itemImage +
                ", itemName='" + itemName + '\'' +
                ", itemPrice=" + itemPrice +
                ", itemDescription='" + itemDescription + '\'' +
                ", itemQuantity=" + itemQuantity +
                ", itemCategorie=" + itemCategories +
                '}';
    }

    public static final Creator<MenuItems> CREATOR = new Creator<MenuItems>() {
        public MenuItems createFromParcel(Parcel in) {
            return new MenuItems(in);
        }

        public MenuItems[] newArray(int size) {
            return new MenuItems[size];
        }
    };


}
