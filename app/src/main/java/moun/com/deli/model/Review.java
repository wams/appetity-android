package moun.com.deli.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by WilliamO on 11/23/2016.
 */
public class Review implements Parcelable{

    private String reviewIdUsuario;
    private String reviewUsuario;
    private String reviewImageUsuario;
    private String reviewRating;
    private String reviewDate;
    private String reviewComents;

    public String getReviewIdUsuario() {
        return reviewIdUsuario;
    }
    public void setReviewIdUsuario(String reviewIdUsuario) {
        this.reviewIdUsuario = reviewIdUsuario;
    }
    public String getReviewUsuario() {
        return reviewUsuario;
    }
    public void setReviewUsuario(String reviewUsuario) {
        this.reviewUsuario = reviewUsuario;
    }
    public String getReviewImageUsuario() {
        return reviewImageUsuario;
    }
    public void setReviewImageUsuario(String reviewImageUsuario) {
        this.reviewImageUsuario = reviewImageUsuario;
    }
    public String getReviewRating() {
        return reviewRating;
    }
    public void setReviewRating(String reviewRating) {
        this.reviewRating = reviewRating;
    }
    public String getReviewDate() {
        return reviewDate;
    }
    public void setReviewDate(String reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getReviewComents() {
        return reviewComents;
    }
    public void setReviewComents(String reviewComents) {
        this.reviewComents = reviewComents;
    }


    public Review() {
        super();
    }

    public Review (String reviewIdUsuario, String reviewUsuario, String reviewImageUsuario, String reviewRating,String reviewDate,String coment){
        this.reviewIdUsuario = reviewIdUsuario;
        this.reviewUsuario = reviewUsuario;
        this.reviewImageUsuario = reviewImageUsuario;
        this.reviewRating = reviewRating;
        this.reviewDate = reviewDate;
        this.reviewComents = coment;
    }

    public Review(Parcel parcel) {
        super();
        this.reviewIdUsuario = parcel.readString();
        this.reviewUsuario = parcel.readString();
        this.reviewImageUsuario = parcel.readString();
        this.reviewRating = parcel.readString();
        this.reviewDate = parcel.readString();
        this.reviewComents = parcel.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(getReviewIdUsuario());
        parcel.writeString(getReviewUsuario());
        parcel.writeString(getReviewImageUsuario());
        parcel.writeString(getReviewRating());
        parcel.writeString(getReviewDate());
        parcel.writeString(getReviewComents());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "Review{" +
                "reviewIdUsuario='" + reviewIdUsuario + '\'' +
                ", reviewUsuario='" + reviewUsuario + '\'' +
                ", reviewImageUsuario='" + reviewImageUsuario + '\'' +
                ", reviewRating='" + reviewRating + '\'' +
                ", reviewDate='" + reviewDate + '\'' +
                ", reviewComents='" + reviewComents + '\'' +
                '}';
    }

    public static final Parcelable.Creator<Review> CREATOR = new Parcelable.Creator<Review>() {
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        public Review[] newArray(int size) {
            return new Review[size];
        }
    };

}
