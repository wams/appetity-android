package moun.com.deli.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import moun.com.deli.fragment.MenuSandwichFragment;
import moun.com.deli.model.MenuItems;

/**
 * Class define a view pager adapter for the swipe tabs feature.
 */

public class MenuPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    ArrayList<MenuItems> itemView;
    String name_cat;
    String[] array_items;

    public MenuPagerAdapter(FragmentManager fm, int NumOfTabs, ArrayList<MenuItems> data,String names[]) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.itemView = data;
       //this.name_cat = name;
         this.array_items = names;
    }

    @Override
    public Fragment getItem(int position) {

     /*   switch (position) {
            case 0:
                // Create a new fragment
                MenuSandwichFragment tab1 = new MenuSandwichFragment();
                return tab1;
            case 1:
                MenuBurgersFragment tab2 = new MenuBurgersFragment();
                return tab2;
            case 2:
                MenuPizzaFragment tab3 = new MenuPizzaFragment();
                return tab3;
            case 3:
                MenuSaladsFragment tab4 = new MenuSaladsFragment();
                return tab4;
            case 4:
                MenuDessertsFragment tab5 = new MenuDessertsFragment();
                return tab5;
            case 5:
                MenuDrinksFragment tab6 = new MenuDrinksFragment();
                return tab6;
            default:
                return null;
        }*/
        MenuSandwichFragment fm = new MenuSandwichFragment();
         String name = array_items[position];
         return fm.newInstance(itemView,name) ;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}
