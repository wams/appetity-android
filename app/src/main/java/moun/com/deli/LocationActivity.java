package moun.com.deli;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import moun.com.deli.Libs.AlmacenamientoLocal;
import moun.com.deli.Parse.Parse_Restaurant;
import moun.com.deli.Parse.Parse_posiciones;
import moun.com.deli.adapter.WindowInfoAdapter;
import moun.com.deli.model.Restaurants;
import moun.com.deli.util.AppUtils;
import moun.com.deli.util.AsyncTaskDrawDirection;

public class LocationActivity extends AppCompatActivity implements
        GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback {
    //GoogleMap.OnMarkerClickListener {
    // ,LocationListener {


    /************************************************************/
    private ArrayList<Restaurants> dataList;
    GoogleMap mMap;
    private Marker branchFour;
    private int once = 1;
    private TextView mTitle;
    private LatLngBounds latlngBounds;
    private Button bNavigation;
    private Polyline newPolyline;
    private double startLat, startLng, endLat, endLng;
    EditText etDestination;
    private ArrayList<Restaurants> locationRes;
    Bitmap bmp;
    Parse_posiciones parseM;
    private String nameRes;
    AlmacenamientoLocal cache;
    String colorMap;
    boolean booleanMap;
    public static final String TAG_DATA_PERMA="";
    Parse_Restaurant mParse;
    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        cache = new AlmacenamientoLocal(this);
        etDestination = (EditText) findViewById(R.id.etDestination);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Bundle b = this.getIntent().getExtras();
        if (b != null) {
            colorMap = b.getString("colorMap");
            booleanMap = b.getBoolean("booleanMap");
            //ArrayList<Restaurants> datosRestaurant = (ArrayList<Restaurants>) getIntent().getSerializableExtra("datosRestaurant");
            if((ArrayList<Restaurants>)getIntent().getSerializableExtra("locRestaurant") == null){

                dataList =  (ArrayList<Restaurants>)getIntent().getSerializableExtra("listRes");
                cache.setVariablePermanente(TAG_DATA_PERMA,dataList.toString());

            }else if((ArrayList<Restaurants>)getIntent().getSerializableExtra("listRes") == null){
                dataList =  (ArrayList<Restaurants>)getIntent().getSerializableExtra("locRestaurant");
                nameRes = b.getString("posRestaurante");
                cache.setVariablePermanente(TAG_DATA_PERMA,dataList.toString());
            }

        if(colorMap == null){
            toolbar.setBackgroundColor(Color.parseColor(cache.getVariablePermanente("colorToolbar")));
        }else{
            toolbar.setBackgroundColor(Color.parseColor(colorMap));
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getString(R.string.locations));
        mTitle.setTypeface(AppUtils.getTypeface(this, AppUtils.FONT_BOLD));
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        try {
            mMap.setMyLocationEnabled(true);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        mMap.getUiSettings().setZoomControlsEnabled(true);
        //mMap.setOnMarkerClickListener(this);
        mMap.setInfoWindowAdapter(new WindowInfoAdapter(getApplicationContext(),dataList));
        mMap.setOnMyLocationChangeListener(myLocationChangeListener);
        if(booleanMap == true){
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    mParse = new Parse_Restaurant();
                    JSONObject dataObj=null;
                    try {
                        dataObj = new JSONObject(cache.getVariablePermanente("Restaurant"));
                    }catch (JSONException e){
                        e.printStackTrace();
                    }

                    ArrayList<Restaurants> data = mParse.parse_Restaurant(dataObj);
                    for(int i=0; i < data.size();i++){
                        if(marker.getTitle().equals(data.get(i).getRestaurantName())){
                            Intent intent = new Intent(LocationActivity.this, MainActivity.class);
                            intent.putExtra("datosRestaurantes", data);
                            intent.putExtra("posRestaurantes", marker.getTitle());
                            startActivity(intent);
                            overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        }
                    }
                }
            });
        }
    }

    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {

        @Override

        public void onMyLocationChange(Location location) {
            startLat = location.getLatitude();
            startLng = location.getLongitude();
            mMap.setMyLocationEnabled(true);
            LatLng loc = new LatLng(startLat, startLng);
            if (mMap != null) {
                if (once == 1) {
                    mMap.clear();
                    addMarkersToMap();

                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 12.0f));
                    once = 0;
                }
            }
        }

    };

    private void addMarkersToMap() {
        addMultipleMarkersResta();
    }

    public void addMultipleMarkersResta() {
        if(nameRes == null){
            ArrayList<String>location = new ArrayList<>();
            parseM= new Parse_posiciones();
            for (int i = 0; i < dataList.size(); i++) {
                String data = dataList.get(i).getRestaurantLocation();
                location = parseM.Parse_locaciones(data);
                for(int j=0; j<location.size();j++){
                    String[] loc = location.get(j).split(",");
                    LatLng locPos = new LatLng(Double.parseDouble(loc[0]),Double.parseDouble(loc[1]));
                    addImageIconthread(locPos, dataList.get(i).getRestaurantName(), dataList.get(i).getRestaurantLogo()+","+dataList.get(i).getRatingBar(), dataList.get(i).getRestaurantLocationIcon());
                }
            }
            dataList.clear();
        }else{
            ArrayList<String>location = new ArrayList<>();
            parseM= new Parse_posiciones();
            for (int i = 0; i < dataList.size(); i++) {
                if(nameRes.equals(dataList.get(i).getRestaurantName())) {
                    String data = dataList.get(i).getRestaurantLocation();
                    location = parseM.Parse_locaciones(data);
                    for (int j = 0; j < location.size(); j++) {
                        String[] loc = location.get(j).split(",");
                        LatLng locPos = new LatLng(Double.parseDouble(loc[0]), Double.parseDouble(loc[1]));
                        addImageIconthread(locPos, dataList.get(i).getRestaurantName(), dataList.get(i).getRestaurantLogo() + "," + dataList.get(i).getRatingBar(), dataList.get(i).getRestaurantLocationIcon());
                    }
                }
            }
        }
    }

    public void addImageIconthread(final LatLng location, final String name, final String snippet, final String urlImage) {
        Thread thread = new Thread(new Runnable() {
            Bitmap bmp;

            @Override
            public void run() {
                URL url;
                try {
                    url = new URL(urlImage);
                    bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        branchFour = mMap.addMarker(new MarkerOptions()
                                .position(location)
                                .title(name)
                                .snippet(snippet)
                                .icon(BitmapDescriptorFactory.fromBitmap(bmp)));
                    }
                });
            }
        });
        thread.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
}