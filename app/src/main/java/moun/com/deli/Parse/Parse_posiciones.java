package moun.com.deli.Parse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by WilliamO on 11/9/2016.
 */
public class Parse_posiciones {

    private ArrayList locaciones = new ArrayList<>();

    public final static String TAG_LOCACION= "location";
    public ArrayList Parse_locaciones(String data){
        this.locaciones.clear();
        try{
            JSONArray dataJSON = new JSONArray(data);
            for (int i = 0; i < dataJSON.length(); i++) {
                JSONObject p = dataJSON.getJSONObject(i);
                String posicion = p.getString(TAG_LOCACION);
                locaciones.add(posicion);
            }
        }catch (JSONException e){
        }
        return this.locaciones;
    }
}
