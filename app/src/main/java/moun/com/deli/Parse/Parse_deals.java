package moun.com.deli.Parse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import moun.com.deli.model.MenuItems;

/**
 * Created by WilliamO on 10/25/2016.
 */
public class Parse_deals {

    // DEALS
    public static final String TAG_ID_RESTAURANT       = "id_restaurant";
    public static final String TAG_DEALS               = "deals";
    public static final String TAG_ID_DEALS            = "id_deal";
    public static final String TAG_NAME_DEALS          = "name_deal";
    public static final String TAG_DETAILS_DEALS       = "details_deal";
    public static final String TAG_IMAGE_DEALS         = "image_deal";
    public static final String TAG_PRICE_DEALS         = "price_deal";
    ArrayList<MenuItems> dealList = new ArrayList<MenuItems>();

    public ArrayList<MenuItems> parseData_Deals(JSONArray jsondata) {
        try {
                        //JSONArray d = new JSONArray(jsondata);
                        for (int i = 0; i < jsondata.length(); i++) {
                            HashMap<String,String> mapDeal = new HashMap();
                            JSONObject p = jsondata.getJSONObject(i);
                            String id_deal = p.getString(TAG_ID_DEALS);
                            int auxId = Integer.parseInt(id_deal);
                            String name_deal = p.getString(TAG_NAME_DEALS);
                            String price_deal = p.getString(TAG_PRICE_DEALS);
                            double auxPrice = Double.parseDouble(price_deal);
                            String image_deal = p.getString(TAG_IMAGE_DEALS);
                            String detail_deal = p.getString(TAG_DETAILS_DEALS);
                            dealList.add(new MenuItems(auxId,name_deal,image_deal,auxPrice,detail_deal));
                        }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return dealList;
    }
}
