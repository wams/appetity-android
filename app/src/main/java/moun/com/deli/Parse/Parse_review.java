package moun.com.deli.Parse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import moun.com.deli.model.Review;

/**
 * Created by WilliamO on 12/1/2016.
 */
public class Parse_review {

    public static final String TAG_ID_USUARIO_REVIEW = "id_usuario";
    public static final String TAG_NAME_USUARIO_REVIEW = "name_usuario";
    public static final String TAG_IMAGEN_USUARIO_REVIEW = "image_usuario";
    public static final String TAG_RATING_REVIEW = "rating";
    public static final String TAG_DATE_REVIEW = "date_review";
    public static final String TAG_COMENTS_REVIEW = "coments";
    ArrayList<Review> listReviews;

    public ArrayList<Review> parseReview(JSONArray jsondata){
        listReviews=new ArrayList<Review>();
        try {
            for (int i = 0; i < jsondata.length(); i++) {
                JSONObject p = jsondata.getJSONObject(i);
                String id_usuario = p.getString(TAG_ID_USUARIO_REVIEW);
                String name_usuario = p.getString(TAG_NAME_USUARIO_REVIEW);
                String imagen_usuario = p.getString(TAG_IMAGEN_USUARIO_REVIEW);
                String rating_review = p.getString(TAG_RATING_REVIEW);
                String date_review = p.getString(TAG_DATE_REVIEW);
                String coment_review = p.getString(TAG_COMENTS_REVIEW);
                listReviews.add(new Review(id_usuario,name_usuario,imagen_usuario,rating_review,date_review,coment_review));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }



        return listReviews;
    }
}
