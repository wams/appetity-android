package moun.com.deli.Parse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import moun.com.deli.model.Restaurants;
import moun.com.deli.model.Review;

/**
 * Created by WilliamO on 9/23/2016.
 */
public class Parse_Restaurant {



    // RESTAURANTS
    public static final String TAG_NAME_RESTAURANT            = "name_restaurant";
    public static final String TAG_ADDRESS_RESTAURANT         = "address_restaurant";
    public static final String TAG_LOGO_RESTAURANT            = "icon_restaurant";
    public static final String TAG_LOGO_LOCATION_RESTAURANT   = "icon_location_restaurant";
    public static final String TAG_HORA_APERTURA_RESTAURANT   = "hora_apertura_restaurant";
    public static final String TAG_HORA_CIERRE_RESTAURANT     = "hora_cierre_restaurant";
    public static final String TAG_CATEGORIES_RESTAURANT      = "categories";
    public static final String TAG_ID_RESTAURANT              = "id_restaurant";
    public static final String TAG_PHONE_RESTAURANT           = "phone_restaurant";
    public static final String TAG_DEALS_RESTAURANT           = "deals";
    public static final String TAG_LOCATION_RESTAURANT        = "location_restaurant";
    public static final String TAG_COLOR_APP_RESTAURANT       = "color_app_restaurant";
    public static final String TAG_COLOR_TEXT_RESTAURANT      = "color_app_text_restaurant";
    public static final String TAG_RATING_RESTAURANT          = "rating_restaurant";
    public static final String TAG_RATING_USER_RESTAURANT     = "rating_users_restaurant";
    public static final String TAG_REVIEW_RESTAURANT          = "reviews_restaurant";

    /********COMENTARIOS********/
    public static final String TAG_USUARIO_ID_REVIEW = "id_usuario";
    public static final String TAG_NAME_USUARIO_REVIEW = "name_usuario";
    public static final String TAG_IMG_USUARIO_REVIEW = "image_usuario";
    public static final String TAG_RATING_REVIEW = "rating";
    public static final String TAG_DATE_REVIEW_REVIEW = "date_review";
    public static final String TAG_COMENTS_REVIEW = "coments";

    ArrayList<Restaurants> restaurantList = new ArrayList<Restaurants>();


    List<Review> hola;

    public ArrayList<Restaurants> parse_Restaurant(JSONObject jsondata){
        restaurantList.clear();
        try {
            JSONArray score = jsondata.getJSONArray("Restaurant");
            if (!score.equals("")) {
                for (int k = 0; k < score.length(); k++) {
                    JSONObject f = score.getJSONObject(k);
                    String id_restaurant       = f.getString(TAG_ID_RESTAURANT);
                    String name_restaurant     = f.getString(TAG_NAME_RESTAURANT);
                    String address_restaurant  = f.getString(TAG_ADDRESS_RESTAURANT);
                    String logo_restaurant     = f.getString(TAG_LOGO_RESTAURANT);
                    String logo_location_restaurant = f.getString(TAG_LOGO_LOCATION_RESTAURANT);
                    String hora_apertura_restaurant = f.getString(TAG_HORA_APERTURA_RESTAURANT);
                    String phone_restaurant     =f.getString(TAG_PHONE_RESTAURANT);
                    String hora_cierre_restaurant   = f.getString(TAG_HORA_CIERRE_RESTAURANT);
                    String categories = f.getString(TAG_CATEGORIES_RESTAURANT);
                    String deals = f.getString(TAG_DEALS_RESTAURANT);
                    String location2 = f.getString(TAG_LOCATION_RESTAURANT);
                    String colorbg = f.getString(TAG_COLOR_APP_RESTAURANT);
                    String colortext = f.getString(TAG_COLOR_TEXT_RESTAURANT);
                    String rating_bar = f.getString(TAG_RATING_RESTAURANT);
                    String rating_users = f.getString(TAG_RATING_USER_RESTAURANT);
                    String review = f.getString(TAG_REVIEW_RESTAURANT);

                    restaurantList.add(new Restaurants(
                            id_restaurant,
                            name_restaurant,
                            logo_restaurant,
                            address_restaurant,
                            logo_location_restaurant,
                            phone_restaurant,
                            hora_apertura_restaurant,
                            hora_cierre_restaurant,
                            categories,
                            deals,
                            location2,
                            colorbg,
                            colortext,
                            rating_bar,
                            rating_users,
                            review
                            )
                    );
                }
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
        return restaurantList;
    }
}
