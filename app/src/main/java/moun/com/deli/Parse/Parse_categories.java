package moun.com.deli.Parse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import moun.com.deli.model.MenuItems;

/**
 * Created by WilliamO on 10/25/2016.
 */
public class Parse_categories {

    //CATEGORIES
    public static final String TAG_CATEGORIE              = "categories";
    public static final String TAG_ID_CATEGORIE           = "id_categorie";
    public static final String TAG_NAME_CATEGORIE         = "name_categorie";
    public static final String TAG_IMAGE_CATEGORIE        = "image_categorie";
    public static final String TAG_ITEMS_CATEGORIE        = "items";
    public static final String TAG_ID_RESTAURANT       = "id_restaurant";

    ArrayList<MenuItems> categorieList = new ArrayList<MenuItems>();

    public ArrayList<MenuItems> parseData_categories(JSONArray jsondata) {

        try {
             //JSONArray d = new JSONArray(jsondata);
             for (int i = 0; i < jsondata.length(); i++) {
                JSONObject p = jsondata.getJSONObject(i);
                String id_categorie   = p.getString(TAG_ID_CATEGORIE);
                String name_categorie = p.getString(TAG_NAME_CATEGORIE);
                String image_categorie = p.getString(TAG_IMAGE_CATEGORIE);
                String items_categorie = p.getString(TAG_ITEMS_CATEGORIE);
                 categorieList.add(new MenuItems(name_categorie,image_categorie,items_categorie));
             }

        }catch (JSONException e) {
            e.printStackTrace();
        }

        return categorieList;
    }

}
