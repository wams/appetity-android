package moun.com.deli.Parse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import moun.com.deli.model.MenuItems;

/**
 * Created by WilliamO on 10/25/2016.
 */
public class Parse_itemsMenu {

    //ITEMS[ARRAY] BY CATEGORIE
    public static final String TAG_ID_CATEGORIE_ITEM            = "id_item";
    public static final String TAG_NAME_CATEGORIE_ITEM          = "name_item";
    public static final String TAG_IMAGE_CATEGORIE_ITEM         = "image_item";
    public static final String TAG_DETAILS_CATEGORIE_ITEM       = "details_item";
    public static final String TAG_PRICE_CATEGORIE_ITEM         = "price_item";

    public static final String TAG_CATEGORIE              = "categories";
    public static final String TAG_ID_CATEGORIE           = "id_categorie";
    public static final String TAG_NAME_CATEGORIE         = "name_categorie";
    public static final String TAG_IMAGE_CATEGORIE        = "image_categorie";
    public static final String TAG_ITEMS_CATEGORIE        = "items";
    public static final String TAG_ID_RESTAURANT       = "id_restaurant";

    ArrayList<MenuItems> itemMenuList = new ArrayList<MenuItems>();

    public ArrayList<MenuItems> parseData_itemsMenu(ArrayList<MenuItems> jsondata,  String id_cat) {
        try {

                        for (int i = 0; i < jsondata.size(); i++) {
                            String id_data = jsondata.get(i).getItemName();
                            if( id_data.equals(id_cat)){
                               String datos = jsondata.get(i).getItemCategories();
                                JSONArray d = new JSONArray(datos);
                                    for (int e = 0; e <=d.length(); e++) {
                                        JSONObject q = d.getJSONObject(e);
                                        String id = q.getString(TAG_ID_CATEGORIE_ITEM);
                                        int auxId = Integer.parseInt(id);
                                        String name = q.getString(TAG_NAME_CATEGORIE_ITEM);
                                        String price = q.getString(TAG_PRICE_CATEGORIE_ITEM);
                                        double auxPrice = Double.parseDouble(price);
                                        String image = q.getString(TAG_IMAGE_CATEGORIE_ITEM);
                                        String details = q.getString(TAG_DETAILS_CATEGORIE_ITEM);
                                        itemMenuList.add(new MenuItems(auxId,name,image,auxPrice,details));
                                    }
                            }
                        }

        }catch (JSONException e) {
            e.printStackTrace();
        }
        return itemMenuList;
    }
}
