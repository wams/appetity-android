package moun.com.deli;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

import moun.com.deli.Libs.AlmacenamientoLocal;
import moun.com.deli.database.UserDAO;
import moun.com.deli.fragment.RestaurantsFragment;
import moun.com.deli.model.Restaurants;
import moun.com.deli.util.AppUtils;
import moun.com.deli.util.SessionManager;

/**
 * Created by WilliamO on 10/20/2016.
 */
public class RestaurantsActivity extends AppCompatActivity implements
        RestaurantsFragment.OnItemSelectedListener{
    public static String name_res;
    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;
    AlmacenamientoLocal cache;
    public static ArrayList<Restaurants> datos;
    private Toolbar mToolbar;
    private TextView mTitle;
    private SessionManager session;
    private UserDAO userDAO;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurants);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbar.setNavigationIcon(android.R.drawable.ic_dialog_map);
        // Initialize the title of Toolbar
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getString(R.string.app_name));
        // Add a custom font to the title
        mTitle.setTypeface(AppUtils.getTypeface(this, AppUtils.FONT_BOLD));
        if (savedInstanceState == null) {
            // Add the main fragment (whatever current layout) to the 'content_fragment' FrameLayout.
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            // Create a new Fragment to be placed in the activity layout
            RestaurantsFragment restaurantFragment = new RestaurantsFragment();
            transaction.replace(R.id.content_fragment_restaurant, restaurantFragment);
            // Commit the transaction
            transaction.commit();
        }


    }
    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_searchview, menu);
        return true;
    }*/
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_searchview, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager)RestaurantsActivity.this.getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(RestaurantsActivity.this.getComponentName()));
        }
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public void onItemSelected(int position) {
        Intent intent = new Intent(RestaurantsActivity.this, MainActivity.class);
       /* for (int i=0; i < datos.size();i++){
            if(name_res.equals(datos.get(i).getRestaurantName())){
                intent.putExtra("datosRestaurantCate",datos.get(i).getRestaurantCategories().toString());
                intent.putExtra("datosRestaurantDeals",datos.get(i).getRestaurantDeals().toString());
                intent.putExtra("datosRestaurantLocation",datos.get(i).getRestaurantLocation()+","+datos.get(i).getRestaurantName() +","+ datos.get(i).getRestaurantPhone()+","+datos.get(i).getRestaurantLocationIcon());
                Log.i("locDatos",datos.get(i).toString());
            }
        }*/
        cache = new AlmacenamientoLocal(this);
        cache.setVariablePermanente("colorToolbar",datos.get(position).getRestaurantColorBG());
        intent.putExtra("datosRestaurantes", datos);
        Log.i("datosRes",datos.toString());
        intent.putExtra("posRestaurantes", datos.get(position).getRestaurantName());
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        startActivity(intent);
        finish();
    }

}
