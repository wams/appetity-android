package moun.com.deli;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

import moun.com.deli.fragment.ValorationFragment;
import moun.com.deli.model.Review;
import moun.com.deli.util.AppUtils;

/**
 * Created by WilliamO on 11/21/2016.
 */
public class valorationActivity extends AppCompatActivity{
    Toolbar mToolbar;
    TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valoration);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        // Initialize the title of Toolbar
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getString(R.string.valoration));
        // Add a custom font to the title
        mTitle.setTypeface(AppUtils.getTypeface(this, AppUtils.FONT_BOLD));

        if (savedInstanceState == null) {
            // Add the main fragment (whatever current layout) to the 'content_fragment' FrameLayout.
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            // Create a new Fragment to be placed in the activity layout
            ValorationFragment valorationFragment = new ValorationFragment();
            transaction.replace(R.id.content_fragment_valoration,valorationFragment);
            // Commit the transaction
            transaction.commit();
        }

        Bundle b = this.getIntent().getExtras();
        if(b!=null){
            ArrayList<Review> listItemsDeals  = b.getParcelableArrayList("coments");
            ValorationFragment.comentarios = listItemsDeals ;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }
}
