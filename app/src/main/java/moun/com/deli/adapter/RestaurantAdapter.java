package moun.com.deli.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import moun.com.deli.MainActivity;
import moun.com.deli.Parse.Parse_Restaurant;
import moun.com.deli.R;
import moun.com.deli.RestaurantsActivity;
import moun.com.deli.model.Restaurants;

/**
 * Created by WilliamO on 10/21/2016.
 */
public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.MyViewHolder> {
    private ClickListener clickListener;
    private Activity activity;
    private ArrayList<Restaurants> mRestModel;
    Parse_Restaurant parseM;
    //private ClickListener clickListener;



    public RestaurantAdapter(Activity a, ArrayList<Restaurants> datos){
        this.activity = a;
        this.mRestModel = datos;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_restaurants_listview, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       /* parseM = new Parse_Restaurant();
        holder.title.setText(mRestModel.get(position).getRestaurantName());
        holder.id.setText(mRestModel.get(position).getId());
        holder.url = mRestModel.get(position).getRestaurantLogo();
        Glide.with(activity)
                .load(holder.url)
                //.placeholder(R.drawable.ic_menu_gallery)
                .into(holder.image);
        */
        final Restaurants model = mRestModel.get(position);
        holder.bind(model);
    }

    @Override
    public int getItemCount() {
        return mRestModel.size();
    }

    /*public void setFilter(ArrayList<Restaurants> restaurantModel) {
        mRestModel = new ArrayList<Restaurants>();
        mRestModel.addAll(restaurantModel);
        notifyDataSetChanged();
        }*/

    public class MyViewHolder extends RecyclerView.ViewHolder   {
        TextView title;
        ImageView image;
        TextView id;
        TextView user_rating;
        String url;
        RatingBar ratingBar;
        public MyViewHolder(final View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.menu_title);
            image = (ImageView) itemView.findViewById(R.id.menu_image);
            id = (TextView) itemView.findViewById(R.id.txt_id_restaurant);
            ratingBar = (RatingBar)itemView.findViewById(R.id.ratingBar);
            user_rating = (TextView)itemView.findViewById(R.id.users_rating);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        RestaurantsActivity.name_res = title.getText().toString();
                        clickListener.itemClicked(itemView, getAdapterPosition());
                        //MainFragment.id_res = id.getText().toString();

                        MainActivity.imagenUrl = url;
                        Log.d("CLICK","CLICK: "+getAdapterPosition());
                    }
                }
            });

        }
        public void bind(Restaurants restaurantModel) {

            title.setText(restaurantModel.getRestaurantName());
            id.setText(restaurantModel.getId());
            url = restaurantModel.getRestaurantLogo();
            Glide.with(activity)
                    .load(url)
                    //.placeholder(R.drawable.ic_menu_gallery)
                    .into(image);
            ratingBar.setRating(Float.parseFloat(restaurantModel.getRatingBar()));
            user_rating.setText("("+restaurantModel.getRatingUser()+")");
            }
    }




    // An interface to Define click listener for the ViewHolder's View from any where.
    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    // An interface to Define click listener for the ViewHolder's View from any where.
    public interface ClickListener {
        public void itemClicked(View view, int position);
    }

}

