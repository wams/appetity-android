package moun.com.deli.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import org.json.JSONArray;

import java.util.ArrayList;

import moun.com.deli.Parse.Parse_deals;
import moun.com.deli.fragment.HotDealsViewPagerFragment;
import moun.com.deli.model.MenuItems;

/**
 * Created by WilliamO on 9/22/2016.
 */
public class HotdealsViewPagerAdapter extends FragmentStatePagerAdapter {

    JSONArray datos;
    Parse_deals mParse;
    //JSONObject datos;

    public HotdealsViewPagerAdapter(Activity a, FragmentManager fm, JSONArray data) {
        super(fm);
        this.datos = data;
        //this.datos = jsondata;
    }
    @Override
    public Fragment getItem(int position) {
        mParse =new Parse_deals();
        ArrayList<MenuItems> auxDeals = mParse.parseData_Deals(datos);
        String imageDeals = auxDeals.get(position).getItemImage();
        //return HotDealsViewPagerFragment.newInstance(arrayList_dataDeals.get(position).get(mParse.TAG_IMAGE_DEALS),datos);
        return HotDealsViewPagerFragment.newInstance(auxDeals,imageDeals) ;
    }

    @Override
    public int getCount() {
        return datos.length();
    }
}
