package moun.com.deli.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import moun.com.deli.R;
import moun.com.deli.model.Review;

/**
 * Created by WilliamO on 12/1/2016.
 */
public class Adapter_review extends RecyclerView.Adapter<Adapter_review.MyViewHolder> {
    private Activity activity;
    private ArrayList<Review> coment;

    public Adapter_review(Activity a, ArrayList<Review> c){
        this.activity = a;
        this.coment = c;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_listview_review, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.mTitle.setText(coment.get(position).getReviewUsuario());
        holder.mRating.setRating(Float.parseFloat(coment.get(position).getReviewRating()));

        Glide.with(activity).load(coment.get(position).getReviewImageUsuario()).asBitmap().centerCrop().into(new BitmapImageViewTarget(holder.mImage) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                holder.mImage.setImageDrawable(circularBitmapDrawable);
            }
        });

        holder.mComent.setText(coment.get(position).getReviewComents());
        holder.mDate.setText(coment.get(position).getReviewDate());
    }

    @Override
    public int getItemCount() {
        return coment.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView mTitle;
        RatingBar mRating;
        TextView mDate;
        ImageView mImage;
        TextView mComent;

        public MyViewHolder(View itemView) {
            super(itemView);
            mTitle =(TextView) itemView.findViewById(R.id.menu_title);
            mRating =(RatingBar) itemView.findViewById(R.id.ratingBar);
            mDate = (TextView)itemView.findViewById(R.id.date_review);
            mImage =(ImageView)itemView.findViewById(R.id.menu_image);
            mComent = (TextView)itemView.findViewById(R.id.txt_coment_review);
        }
    }
}
