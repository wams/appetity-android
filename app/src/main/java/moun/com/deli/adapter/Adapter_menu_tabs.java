package moun.com.deli.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import moun.com.deli.fragment.MenuPizzaFragment;

/**
 * Created by WilliamO on 9/28/2016.
 */
public class Adapter_menu_tabs extends FragmentPagerAdapter {

    final int PAGE_COUNT = 6;
    private String[] categories_tabs;
    public Adapter_menu_tabs(FragmentManager fm, String[] categorie) {
        super(fm);
        this.categories_tabs = categorie;
    }

    @Override
    public int getCount() {
        return  categories_tabs.length;
    }

    @Override
    public Fragment getItem(int position) {

        return new MenuPizzaFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return categories_tabs[position];
    }


}