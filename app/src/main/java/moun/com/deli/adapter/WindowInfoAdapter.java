package moun.com.deli.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

import moun.com.deli.R;
import moun.com.deli.model.Restaurants;

/**
 * Created by WilliamO on 11/16/2016.
 */
public class WindowInfoAdapter implements GoogleMap.InfoWindowAdapter {
    ArrayList<Restaurants> restaurantModel;
    LayoutInflater inflater;
    Context context;
    public WindowInfoAdapter(Context context, ArrayList<Restaurants> model){
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.restaurantModel = model;
    }

    @Override
    public View getInfoContents(final Marker marker) {
        View myContentsView = inflater.inflate(R.layout.infowindows, null);
        TextView title      = ((TextView)myContentsView.findViewById(R.id.menu_title));
        ImageView imagen    = (ImageView)myContentsView.findViewById(R.id.menu_image) ;
        RatingBar rating    = (RatingBar)myContentsView.findViewById(R.id.ratingBar);
        String aux = marker.getSnippet();
        String[] datos = aux.split(",");
        rating.setRating(Float.parseFloat(datos[1]));

        /*
        TextView tvSnippet = ((TextView)myContentsView.findViewById(R.id.snippet));
        tvSnippet.setText(marker.getSnippet());*/
        Glide.with(context).load(datos[0]).asBitmap().override(50,50).listener(new RequestListener<String, Bitmap>() {
            @Override
            public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                e.printStackTrace();
                return false;
            }
            @Override
            public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                if(!isFromMemoryCache) marker.showInfoWindow();
                return false;
            }
        }).into(imagen);
        title.setText(marker.getTitle());
        return myContentsView;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        // TODO Auto-generated method stub
        return null;
    }


}
