package moun.com.deli;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import moun.com.deli.Libs.AlmacenamientoLocal;
import moun.com.deli.Parse.Parse_categories;
import moun.com.deli.model.MenuItems;
import moun.com.deli.util.AppUtils;
import moun.com.deli.util.MenuPagerAdapter;

/**
 * An Activity handling two custom {@link android.support.v4.app.Fragment}s,
 * with {@link TabLayout} and {@link ViewPager}.
 */
public class MenuActivityWithTabs extends AppCompatActivity {

    FloatingActionButton fab;
    private TextView mTitle;
    JSONObject jsonData;
    JSONArray jsondataView;
    ArrayList<MenuItems> dataTabs;
    Parse_categories parseM;
    String name_categorie="sandwich";
    String[] array_name;
    ArrayList<MenuItems> data;
    ArrayList<MenuItems> deals;
    AlmacenamientoLocal cache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        cache = new AlmacenamientoLocal(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.parseColor(cache.getVariablePermanente("colorToolbar")));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getString(R.string.our_menu));
        mTitle.setTypeface(AppUtils.getTypeface(this, AppUtils.FONT_BOLD));

        Bundle b = this.getIntent().getExtras();
        if(b!=null){
            //data= b.getParcelableArrayList("listMenuItems");
            data = (ArrayList<MenuItems>) getIntent().getSerializableExtra("listMenuItems");
            deals = (ArrayList<MenuItems>) getIntent().getSerializableExtra("dataDeals");

            String positionCurrent = b.getString("currentItem");
            String aux = b.getString("name_categorie");
            /*if(aux == ""){
                this.name_categorie = aux;
            }
            //String name_menu = b.getString("name");
            try{
                this.jsonData = new JSONObject(data);
                //this.jsondataView = new JSONArray(items_data);
            }catch (JSONException e){
                e.printStackTrace();
            }*/
    }
        parseM = new Parse_categories();
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        //dataTabs = parseM.parseData_categories(jsonData);
        array_name = new String[data.size()];
       /*for(int i =0; i < dataTabs.size();i++){
            tabLayout.addTab(tabLayout.newTab().setText(dataTabs.get(i).get(parseM.TAG_NAME_CATEGORIE)));
            array_name[i] = dataTabs.get(i).get(parseM.TAG_NAME_CATEGORIE);
        }*/
        for(int i= 0; i < data.size(); i++){
            tabLayout.addTab(tabLayout.newTab().setText(data.get(i).getItemName()));
            array_name[i] = data.get(i).getItemName();
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final MenuPagerAdapter adapter = new MenuPagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount(),data,array_name);
        viewPager.setAdapter(adapter);
        int i = getIntent().getIntExtra("currentItem", 1);
        viewPager.setCurrentItem(i-1);

       /* if (i == 2) {
            viewPager.setCurrentItem(1);
        }
        if (i == 3) {
            viewPager.setCurrentItem(2);
        }
        if (i == 4) {
            viewPager.setCurrentItem(3);
        }
        if (i == 5) {
            viewPager.setCurrentItem(4);
        }
        if (i == 6) {
            viewPager.setCurrentItem(5);
        }*/
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor(cache.getVariablePermanente("colorToolbar")));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override

            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(cache.getVariablePermanente("colorToolbar"))));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivityWithTabs.this, MyCartActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_our_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId())
        {
            case R.id.menu_hot:
                Intent intent = new Intent(MenuActivityWithTabs.this,HotDealsActivity.class);
                intent.putExtra("dataDeals",deals);
                startActivity(intent);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }
}
