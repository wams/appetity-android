package moun.com.deli;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mercadopago.core.MercadoPago;
import com.mercadopago.exceptions.MPException;
import com.mercadopago.model.CheckoutPreference;
import com.mercadopago.model.Issuer;
import com.mercadopago.model.PaymentMethod;
import com.mercadopago.model.Token;
import com.mercadopago.util.JsonUtil;

import org.json.JSONException;
import org.json.JSONObject;

import moun.com.deli.Conection.ConexionHTTP;
import moun.com.deli.Conection.DataCallback;
import moun.com.deli.Libs.ConnectionDetector;
import moun.com.deli.Parse.Parse_preference;
import moun.com.deli.mercadopago.mpActivity;
import moun.com.deli.mercadopago.utils.ExamplesUtils;
import moun.com.deli.util.AppUtils;

/**
 * Created by WilliamO on 12/27/2016.
 */
public class PayModeActivity extends AppCompatActivity implements DataCallback {
    Toolbar mToolbar;
    TextView mTitle;
    ImageView img_mercadoPago;
    ConnectionDetector conexion;
    ImageView img_checkout;
    JSONObject dataJson;
    JSONObject dataParseada;

    private CheckoutPreference mCheckoutPreference;
    private String mPublicKey = "TEST-897d60bd-d324-4106-9a63-5e311f50aa79";
    private Activity mActivity;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paymode);
        conexion = new ConnectionDetector(this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        // Initialize the title of Toolbar
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getString(R.string.title_paymod));
        mTitle.setTypeface(AppUtils.getTypeface(this, AppUtils.FONT_BOLD));

        img_mercadoPago = (ImageView)findViewById(R.id.image_mp);
        //img_checkout = (ImageView)findViewById(R.id.image_transf);
        mActivity = this;

        img_mercadoPago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent exampleIntent = new Intent(PayModeActivity.this, mpActivity.class);
                startActivity(exampleIntent);
            }
        });
       /* img_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //checkOutBasico();
            }
        });*/
    }

    // Método ejecutado al hacer clic en el botón
    public void submit(View view) {

        String url_api_wams = "http://appetity.azurewebsites.net/create_preference";
        String irl_mercadopago_exam="http://private-4d9654-mercadopagoexamples.apiary-mock.com/merchantUri/create_preference";
        if (conexion.hasInternet(this)) {
            try {
                ConexionHTTP.getWebData(url_api_wams,this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
        }

        /********************************_LLAMADO AL CHECKOUT BASICO_****************************************/

        // Crea un mapa con los datos de la compra y el mail de tu cliente.
        /*Map<String, Object> preferenceMap = new HashMap<>();
        preferenceMap.put("title", "Estoy Probando");
        preferenceMap.put("quantity", "5");
        preferenceMap.put("currency_id", "VEF");
        preferenceMap.put("unit_price", new BigDecimal(30));
        preferenceMap.put("id", "1");


        // Envía la información a tu servidor
        MerchantServer.createPreference(this, "http://appetity.azurewebsites.net",
                "/create_preference", preferenceMap, new Callback<CheckoutPreference>() {
                    @Override
                    public void success(CheckoutPreference checkoutPreference) {
                        // La preferencia se creó correctamente.
                        // Aquí puedes iniciar el Checkout de MercadoPago.
                        new MercadoPago.StartActivityBuilder()
                                .setActivity(mActivity)
                                .setPublicKey("TEST-897d60bd-d324-4106-9a63-5e311f50aa79")
                                .setCheckoutPreferenceId(checkoutPreference.getId())
                                .startCheckoutActivity();
                    }

                    @Override
                    public void failure(ApiException error) {
                        LayoutUtil.showRegularLayout(mActivity);
                        Toast.makeText(mActivity, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        /********************************_LLAMADO AL CHECKOUT PERZONALIDO_****************************************/
        /*Intent intent = new Intent(this,mpActivity.class);
        startActivity(intent);*/
    }

    // Espera los resultados del checkout
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {

        if(requestCode == MercadoPago.CARD_VAULT_REQUEST_CODE) {
            if(resultCode == RESULT_OK) {
                PaymentMethod paymentMethod = JsonUtil.getInstance().fromJson(data.getStringExtra("paymentMethod"), PaymentMethod.class);
                Issuer issuer = JsonUtil.getInstance().fromJson(data.getStringExtra("issuer"), Issuer.class);
                Token token = JsonUtil.getInstance().fromJson(data.getStringExtra("token"), Token.class);
                // Done! Send the data to your server
                ExamplesUtils.createPayment(this, data.getStringExtra("token"),
                        1, null, JsonUtil.getInstance().fromJson(data.getStringExtra("paymentMethod"), PaymentMethod.class), null);
            } else {
                if ((data != null) && (data.hasExtra("mpException"))) {
                    MPException exception = JsonUtil.getInstance()
                            .fromJson(data.getStringExtra("mpException"), MPException.class);
                }
            }
        }
    }

    @Override
    public void callback(String data) {
        Parse_preference mParse = new Parse_preference();

        try{
           dataJson = new JSONObject(data);
           dataParseada = dataJson.getJSONObject("response");
        }catch (JSONException e){
            e.printStackTrace();
        }
        Gson gson = new Gson();
        CheckoutPreference preference = gson.fromJson(dataParseada.toString(), CheckoutPreference.class);
        new MercadoPago.StartActivityBuilder()
                .setActivity(mActivity)
                .setPublicKey("TEST-897d60bd-d324-4106-9a63-5e311f50aa79")
                .setCheckoutPreferenceId(preference.getId())
                .startCheckoutActivity();
    }
}
